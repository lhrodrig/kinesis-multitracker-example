# Learning Amazon Kinesis Development

The master branch provides completed code for the [Process Real-Time Stock Data Using KPL and KCL Tutorial][learning-kinesis]  in the [Kinesis Developer Guide][kinesis-developer-guide].

The tutorial uses KCL 2.2.9 to demonstrate how to send a stream of records to Kinesis Data Streams and implement an application that consumes and processes the records in near-real time. 

[learning-kinesis]:  https://docs.aws.amazon.com/streams/latest/dev/tutorial-stock-data-kplkcl.html
[kinesis-developer-guide]: http://docs.aws.amazon.com/kinesis/latest/dev/introduction.html


The writer class acts as a producer 
Modified project to consume from different streams 


## Steps
- Have the AWS credentials set up in your local environment (make sure it has the permissions to handle all kinesis actions and also for DynamoDB) [Docs](https://docs.aws.amazon.com/streams/latest/dev/tutorial-stock-data-kplkcl2-iam.html)
- Create two kinesis data-streams (provisioned with one shard)
- Initiate the stock processor 
- Write data to the streams with the StockTradesWriter Class

## License Summary

This sample code is made available under the MIT-0 license. See the LICENSE file.
