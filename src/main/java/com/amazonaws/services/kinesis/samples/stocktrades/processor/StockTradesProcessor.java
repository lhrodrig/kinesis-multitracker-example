/*
 * Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/asl/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.amazonaws.services.kinesis.samples.stocktrades.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.cloudwatch.CloudWatchAsyncClient;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;
import software.amazon.kinesis.common.*;
import software.amazon.kinesis.coordinator.Scheduler;
import software.amazon.kinesis.processor.FormerStreamsLeasesDeletionStrategy;
import software.amazon.kinesis.processor.MultiStreamTracker;

/**
 * Uses the Kinesis Client Library (KCL) 2.2.9 to continuously consume and process stock trade
 * records from the stock trades stream. KCL monitors the number of shards and creates
 * record processor instances to read and process records from each shard. KCL also
 * load balances shards across all the instances of this processor.
 *
 */
public class StockTradesProcessor {
    private static final Log LOG = LogFactory.getLog(StockTradesProcessor.class);
    private static final Logger ROOT_LOGGER = Logger.getLogger("");
    private static final Logger PROCESSOR_LOGGER =
            Logger.getLogger("com.amazonaws.services.kinesis.samples.stocktrades.processor.StockTradeRecordProcessor");
    private static void checkUsage(String[] args) {
        if (args.length != 3) {
            System.err.println("Usage: " + StockTradesProcessor.class.getSimpleName()
                    + " <application name> <stream name> <region>");
            System.exit(1);
        }
    }

    /**
     * Sets the global log level to WARNING and the log level for this package to INFO,
     * so that we only see INFO messages for this processor. This is just for the purpose
     * of this tutorial, and should not be considered as best practice.
     *
     */
    private static void setLogLevels() {
        ROOT_LOGGER.setLevel(Level.INFO);
        // Set this to INFO for logging at INFO level. Suppressed for this example as it can be noisy.
        PROCESSOR_LOGGER.setLevel(Level.INFO);
    }

    public static void main(String[] args) throws Exception {
        checkUsage(args);
        setLogLevels();
        String applicationName = args[0];
        String streamName = args[1];
        Region region = Region.of(args[2]);
        if (region == null) {
            System.err.println(args[2] + " is not a valid AWS region.");
            System.exit(1);
        }
        // KINESIS BUILDER PROPERTIES
        KinesisAsyncClient kinesisClient = KinesisClientUtil.createKinesisAsyncClient(KinesisAsyncClient.builder().region(region));
        DynamoDbAsyncClient dynamoClient = DynamoDbAsyncClient.builder().region(region).build();
        CloudWatchAsyncClient cloudWatchClient = CloudWatchAsyncClient.builder().region(region).build();
        StockTradeRecordProcessorFactory shardRecordProcessor = new StockTradeRecordProcessorFactory();
        KinesisStreamConfigList multi = new KinesisStreamConfigList();
        //ConfigsBuilder configsBuilder = new ConfigsBuilder(streamName, applicationName, kinesisClient, dynamoClient, cloudWatchClient, UUID.randomUUID().toString(), shardRecordProcessor);
        ConfigsBuilder configsBuilder2 = new ConfigsBuilder(multi, applicationName, kinesisClient, dynamoClient, cloudWatchClient,UUID.randomUUID().toString(), shardRecordProcessor);
        Scheduler scheduler = new Scheduler(
                configsBuilder2.checkpointConfig(),
                configsBuilder2.coordinatorConfig(),
                configsBuilder2.leaseManagementConfig(),
                configsBuilder2.lifecycleConfig(),
                configsBuilder2.metricsConfig(),
                configsBuilder2.processorConfig(),
                configsBuilder2.retrievalConfig()
        );
        int exitCode = 0;
        try {
            scheduler.run();
        } catch (Throwable t) {
            LOG.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        System.exit(exitCode);

    }

}


// class that implements the MultiStreamTracker interface
// the class ConfigsBuilder can accept both a stream name or a class that implements the interface
class KinesisStreamConfigList implements MultiStreamTracker{
    private static  final  String STREAM_IDENTIFIER_1 = ""; // account-id:streamName:epoch
    private static  final  String STREAM_IDENTIFIER_2 = "";
    @Override
    public List<StreamConfig> streamConfigList() {
        //LIST OF KINESIS STREAMS
        List<StreamConfig> kinesisStreamsConfig = new ArrayList<>();
        StreamConfig stream1 = new StreamConfig(
                StreamIdentifier.multiStreamInstance(STREAM_IDENTIFIER_1),
                InitialPositionInStreamExtended.newInitialPosition(InitialPositionInStream.TRIM_HORIZON)
        );
        StreamConfig stream2 = new StreamConfig(
                StreamIdentifier.multiStreamInstance(STREAM_IDENTIFIER_2),
                InitialPositionInStreamExtended.newInitialPosition(InitialPositionInStream.TRIM_HORIZON)
        );
        kinesisStreamsConfig.add(stream1);
        kinesisStreamsConfig.add(stream2);
        return kinesisStreamsConfig;
    }

    @Override
    public FormerStreamsLeasesDeletionStrategy formerStreamsLeasesDeletionStrategy() {
        return new FormerStreamsLeasesDeletionStrategy.NoLeaseDeletionStrategy();
    }
}
